import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    products: [
      {
        id: 1,
        price: 120,
        src: "https://picsum.photos/400/400/?image=21",
        name: "zapatardos",
        description: "para los chiquis",
        stock: 10,
        tagSet: ['malardo', 'buenardo', 'meCeba']
      },
      {
        id: 2,
        price: 120,
        src: "https://picsum.photos/400/400/?image=1000",
        name: "Viaje a FONTANA",
        description: "para los grandes",
        stock: 2,
        new: true,
        tagSet: ['cafeLaHumedad', 'amarlosPeroNoTanto', 'de0a100']
      }
      ,
      {
        id: 3,
        price: 121,
        src: "https://picsum.photos/400/400/?image=24",
        name: "ropita",
        description: "para gente rica ",
        new: true, 
        stock: 100,
        tagSet: ['cafeLaHumedad', 'amarlosPeroNoTanto', 'de0a100']
      },
      {
        id: 4,
        price: 189,
        src: "https://picsum.photos/400/400/?image=10",
        name: "linea 5 trip",
        description: "para los cracks",
        new: true,
        stock: 14,
        tagSet: ['chiquis', 'amarlos', 'saludables', 'bombonazo']
      },
      {
        id: 5,
        price: 432,
        src: "https://picsum.photos/400/400/?image=1",
        name: "pc i10",
        stock: 23,
        description: "para los gamers",
        tagSet: ['cafeLaHumedad', 'amarlosPeroNoTanto', 'de0a100']
      }
    ],
    tagSpaces: [
      {
        id: 1,
        tagSet: ['chiquis', 'amarlos', 'saludables', 'bombonazo']
      }
      ,
      {
        id: 2,
        tagSet: ['Tremendaaa','chiquis', 'amarlos']
      }
      ,
      {
        id: 3,
        tagSet: ['chiquis', 'amarlos', 'saludables', 'bombonazo']
      }
      ,
      {
        id: 4,
        tagSet: ['chiquis', 'amarlos', 'saludables', 'bombonazo']
      }
      ,
      {
        id: 5,
        tagSet: ['malardo', 'buenardo', 'meCeba']
      }
      ,
      {
        id: 6,
        tagSet: ['cafeLaHumedad', 'amarlosPeroNoTanto', 'de0a100']
      }
    ]
  },
  mutations: {
    agregarProducto: (state, product) => (state.products.unshift(product))
  },
  actions: {
    agregarProducto({ commit }, product) {
      commit('agregarProducto', product)
    }
    // Ejemplo de Acciones ASINCRONAS CON ASYNC AWAIT
    // ,
    // async actionA ({ commit }) {
    //   commit('gotData', await getData())
    // },
    // async actionB ({ dispatch, commit }) {
    //   await dispatch('actionA') // wait for `actionA` to finish
    //   commit('gotOtherData', await getOtherData())
  }
  ,
  getters: {
    nuevos: state => {
      return state.products.filter(product => product.new)
    },
    getProductById: state => id => { return state.products.filter(product => product.id === id) },
    getProductByTagSet: state => tagSet => {
      return state.products.filter(product => new Set([...new Set(product.tagSet)].filter(x => new Set(tagSet).has(x))).size > 0)
    }
  }
  //   // ,
  //   // tagSpace: (state) => (id) => {
  //   //   return state.tagSpaces.filter(tagSpace => tagSpace.id === id)
  //   // }


})
